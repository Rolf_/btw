from flask import Flask, request, render_template

# start the application
app = Flask(__name__)


@app.route('/', methods=['GET', 'POST'])
def bereken_btw():
    if request.method == 'GET':
        return render_template('btw.html')
    elif request.method == 'POST':
        bedrag = request.form['bedrag']
        incl = request.form['incl']
        btw = request.form['btw']
        if ',' in bedrag:
            bedrag = float(bedrag.replace(',', '.'))
        else:
            bedrag = float(bedrag)

        # test if btw format need to change
        if '%' in btw:
            print('yes')
            btw = btw.replace('%', '')
        if ',' in btw:
            print('yes, yes')
            btw = btw.replace(',', '.')
        btw = float(btw)

        #################################
        # variabelen voor de berekening
        btw_excl = btw / 100.0
        btw_incl = btw / (btw + 100.0)
        exclbtw = 0
        btwbedrag = 0
        inclbtw = 0

        #################################
        # bereken de BTW voor een bedrag inclusief BTW
        if incl == 'Check':
            inclbtw = bedrag
            btwbedrag = inclbtw * btw_incl
            exclbtw = inclbtw - btwbedrag

        #################################
        # bereken BTW voor een bedrag exclusief BTW
        elif incl == 'NoCheck':
            exclbtw = bedrag
            btwbedrag = exclbtw * btw_excl
            inclbtw = exclbtw + btwbedrag
        return render_template('resultaat.html', exclbtw='{:>.2f}'.format(exclbtw).replace('.', ','),
                               btwbedrag='{:>.2f}'.format(btwbedrag).replace('.', ','),
                               inclbtw='{:>.2f}'.format(inclbtw).replace('.', ','),
                               bedrag='{:>.2f}'.format(bedrag).replace('.', ','))


if __name__ == '__main__':
    app.run()
